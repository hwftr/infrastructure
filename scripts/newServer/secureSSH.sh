echo "Only run if you copied the SSH-Key!!!"
cp /etc/ssh/sshd_config /etc/ssh/sshd_config.backup
echo "Created SSH-Config Backup..."
rm /etc/ssh/sshd_config
apt install wget -y
wget https://gitlab.com/hwftr/infrastructure/-/raw/master/scripts/newServer/configs/etc/ssh/sshd_config 
mv sshd_config /etc/ssh/sshd_config

